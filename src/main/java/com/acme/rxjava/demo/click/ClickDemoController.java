package com.acme.rxjava.demo.click;

import io.reactivex.Observable;
import io.reactivex.rxjavafx.observables.JavaFxObservable;
import io.reactivex.rxjavafx.schedulers.JavaFxScheduler;
import io.reactivex.schedulers.Schedulers;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class ClickDemoController {

    @FXML
    private Label clickCountLabel;

    @FXML
    private Button clickMeButton;

    @FXML
    private void initialize() {
        registerClickObserver();
    }

    private void registerClickObserver() {
        Observable<ActionEvent> clickObservable = JavaFxObservable
                .actionEventsOf(clickMeButton);
        /*
        * Execution flow:
        * 1-) Switch to a thread from the computation thread pool
        * 2-) Buffer events until eventObservable#debounce emits an event
        * 3-) Map the event list to its size (map is often used to transform one kind of data into another)
        * 4-) Switch to the JavaFx main thread
        * 5-) Update clickCountLabel
        */
        clickObservable
                .observeOn(Schedulers.computation())
                .buffer(clickObservable.debounce(250, TimeUnit.MILLISECONDS))
                .map(List::size)
                .observeOn(JavaFxScheduler.platform())
                .subscribe(e -> clickCountLabel.setText("Number of clicks: " + e));
    }

}
