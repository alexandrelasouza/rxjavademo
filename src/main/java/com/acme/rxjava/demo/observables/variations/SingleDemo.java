package com.acme.rxjava.demo.observables.variations;

import io.reactivex.Single;
import io.reactivex.disposables.Disposable;

public class SingleDemo {

    public static void main(String[] args) {
        // Single has either a single element or throws an error
        //
        // Observer is disposed after completion or error
        Single<Integer> okSingle = Single.fromCallable(() -> 1); // OK Single!
        Single<Integer> nokSingle = Single.fromCallable(() -> null); // NOK Single

        Disposable d1 = okSingle.subscribe(x -> System.out.println("OK Single: onSuccess"));
        Disposable d2 = nokSingle
                .subscribe(x -> System.out.println("NOK Single: onSuccess"),
                        t -> System.out.println("NOK Single -> Thrown error: " + t.getMessage()));

        System.out.println("D1 disposed: " + d1.isDisposed());
        System.out.println("D2 disposed: " + d2.isDisposed());
    }

}
