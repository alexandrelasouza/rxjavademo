package com.acme.rxjava.demo.observables.variations;

import io.reactivex.Completable;
import io.reactivex.disposables.Disposable;

public class CompletableDemo {

    public static void main(String[] args) {

        // Observer is disposed after executing.
        //
        // Subscribers for completable do not receive the items that can be emitted.
        // They only care about the completion or error!
        // Note: the Action/Runnable/Callable specified below will execute for EACH subscriber
        Completable completable = Completable.fromAction(() -> {
            System.out.println("I'm processing too many things");
            Thread.sleep(2000);
        });

        Disposable d1 = completable.subscribe(() -> System.out.println("Action finished processing"));
        System.out.println("D1 disposed: " + d1.isDisposed());

        Disposable d2 = completable.subscribe(() -> System.out.println("Action finished processing"));
        System.out.println("D2 disposed: " + d2.isDisposed());
    }

}
