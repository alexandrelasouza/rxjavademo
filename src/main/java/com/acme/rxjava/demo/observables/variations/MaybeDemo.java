package com.acme.rxjava.demo.observables.variations;

import io.reactivex.Maybe;
import io.reactivex.disposables.Disposable;

public class MaybeDemo {

    public static void main(String[] args) {

        // Maybe has either:
        // - a single item
        // - no item (or null value). "No item" is achievable using fromAction(Consumer)
        // - error
        //
        // Observer disposed after emitting item or error
        Maybe<Integer> maybe1 = Maybe.fromCallable(() -> 1);
        Maybe<Integer> maybe2 = Maybe.fromCallable(() -> null);
        Maybe<Integer> maybe3 = Maybe.fromAction(() -> {
           throw new UnsupportedOperationException();
        });

        Disposable d1 = maybe1.subscribe(x -> System.out.println("D1 onSuccess: " + x),
                t -> System.out.println("D1 onError: " + t.getMessage()),
                () -> System.out.println("D1 onComplete")); // Executes onSuccess

        Disposable d2 = maybe2.subscribe(x -> System.out.println("D2 onSuccess: " + x),
                t -> System.out.println("D2 onError: " + t.getMessage()),
                () -> System.out.println("D2 onComplete")); // Executes onComplete because value is null

        Disposable d3 = maybe3
                .subscribe(x -> System.out.println("D3 onSuccess: " + x),
                        t -> System.out.println("D3 onError: " + t.getMessage()),
                        () -> System.out.println("D2 onComplete")); // Executes onError due to the thrown Exception

        System.out.println("D1 disposed: " + d1.isDisposed());
        System.out.println("D2 disposed: " + d2.isDisposed());
        System.out.println("D3 disposed: " + d3.isDisposed());
    }

}
