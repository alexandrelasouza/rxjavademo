package com.acme.rxjava.demo.bus;

import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import java.io.IOException;

public class MainEventBus {

    public static void main(String[] args) {
        long threadId = Thread.currentThread().getId();

        System.out.println("main method thread: " + threadId);

        IntegerEventBus eventBus = IntegerEventBus.getInstance();

        Integer[] data = {
          1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20
        };

        Disposable clientWithBuffer = eventBus.asObservable()
                .subscribe(events -> {
                    long clientThreadId = Thread.currentThread().getId();
                    System.out.println("MainThreadClient   | thread #" + clientThreadId + ": " + events);
                });

        Disposable backgroundClient = eventBus.asObservable()
                .observeOn(Schedulers.computation())
                .subscribe(events -> {
                    long clientThreadId = Thread.currentThread().getId();
                    System.out.println("BackgroundClient1  | thread #" + clientThreadId + ": " + events);
                });

        Disposable backgroundClient2 = eventBus.asObservable()
                .observeOn(Schedulers.computation())
                .subscribe(events -> {
                    long clientThreadId = Thread.currentThread().getId();
                    System.out.println("BackgroundClient2  | thread #" + clientThreadId + ": " + events);
                });

        for (Integer value : data) {
            eventBus.send(value);
        }

        try {
            System.out.println("Sleeping main thread");
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        clientWithBuffer.dispose();
        backgroundClient.dispose();
        backgroundClient2.dispose();
    }

}
