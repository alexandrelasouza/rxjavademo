package com.acme.rxjava.demo.bus;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

/* package */ class IntegerEventBus {

    private static final IntegerEventBus INSTANCE = new IntegerEventBus();

    private PublishSubject<Integer> mPublisher;

    private IntegerEventBus() {
        mPublisher = PublishSubject.create();
    }

    /* package */ void send(Integer event) {
        mPublisher.onNext(event);
    }

    /* package */ Observable<Integer> asObservable() {
        return mPublisher;
    }

    /* package */ static IntegerEventBus getInstance() {
        return INSTANCE;
    }

}
