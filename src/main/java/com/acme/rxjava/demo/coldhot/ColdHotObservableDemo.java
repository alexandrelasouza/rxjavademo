package com.acme.rxjava.demo.coldhot;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observables.ConnectableObservable;

public class ColdHotObservableDemo {

    public static void main(String[] args) {

        CompositeDisposable compositeDisposable = new CompositeDisposable();

        Observable<Integer> hotIntStream = Observable.fromArray(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

        System.out.println("Subscribing to hot");
        compositeDisposable.add(hotIntStream.subscribe(x -> System.out.println("Hot: " + x)));

        ConnectableObservable<Integer> coldIntStream = hotIntStream.publish();

        System.out.println("Adding subscriber before connect");
        compositeDisposable.add(coldIntStream
                .subscribe(x -> System.out.println("Cold before connect: " + x)));

        System.out.println("Calling connect for the observer registered");
        coldIntStream.connect();

        System.out.println("Calling connect for the observer registered AFTER the first connect() call");
        compositeDisposable.add(coldIntStream.subscribe(x -> System.out.println("Cold after connect: " + x)));
        coldIntStream.connect();

        compositeDisposable.dispose();
    }

}
