package com.acme.rxjava.demo.operator;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;

public class BufferOperatorDemo {

    public static void main(String[] args) {

        // Emits the buffer (a list of items) when the N
        // value is reached.
        Disposable disposable = Observable.fromArray(1, 2, 3, 4, 5, 6)
                .buffer(2)
                .subscribe(System.out::println);

        disposable.dispose();
    }

}
