package com.acme.rxjava.demo.operator;

import io.reactivex.Observable;

public class TakeOperatorDemo {

    public static void main(String[] args) {

        // The take operator automatically disposes the
        // observer once the N elements have been emitted
        Observable<Integer> intStream = Observable.fromArray(1, 2, 3, 4, 5, 6);


        // Emits only the first two elements
        intStream
                .take(2)
                .subscribe(x -> System.out.println("First observer: " + x));

        // Emits only the first three elements
        intStream
                .take(3)
                .subscribe(x -> System.out.println("Second observer: " + x));
    }

}
