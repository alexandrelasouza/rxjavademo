package com.acme.rxjava.demo.operator;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;

public class GroupByOperator {

    private static final String EVEN_KEY = "Even";
    private static final String ODD_KEY = "Odd";

    public static void main(String[] args) {
        Observable<Integer> intStream = Observable.fromArray(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

        // GroupBy will call the function in subscribe when there is no group
        // created for the given key.
        //
        // In this function, we define an observer to the group.
        // GroupedObservable supports a single observer during its life time.
        //
        // Here we will have only two groups: Even and Odd
        // Therefore, the function in the subscribe will be called only ONCE for each key
        Disposable disposable = intStream
                .groupBy(x -> x%2 == 0 ? EVEN_KEY : ODD_KEY) // Will split data into odd and even numbers groups
                .subscribe(groupObservable -> {
                    String groupKey = groupObservable.getKey();
                    groupObservable
                            .subscribe(x -> System.out.println("Key: " + groupKey + ". Value: " + x));
                });

        disposable.dispose();
    }

}
