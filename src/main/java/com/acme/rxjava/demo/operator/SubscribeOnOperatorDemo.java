package com.acme.rxjava.demo.operator;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class SubscribeOnOperatorDemo {

    public static void main(String[] args) {

        // SubscribeOn defines the thread in which the Observable will operate.
        // If no ObserveOn operators are used, Observer will run in the same thread as the Observable
        long threadId = Thread.currentThread().getId();
        System.out.println("Main Thread ID: " + threadId);

        Disposable d1 = Observable
                .fromCallable(() -> {
                    long runningThread = Thread.currentThread().getId();
                    System.out.println("D1 Callable thread ID: " + runningThread);

                    return 1;
                })
                .subscribe(x -> {
                    long observerRunningThread = Thread.currentThread().getId();
                    System.out.println("D1. Thread ID: " + observerRunningThread + ". Item: " + x);
                }); // Runs on the main program thread. Callable also runs on the main thread

        Disposable d2 = Observable
                .fromCallable(() -> {
                    long runningThread = Thread.currentThread().getId();
                    System.out.println("D2 Callable thread ID: " + runningThread);

                    return 1;
                })
                .subscribeOn(Schedulers.newThread()) // The thread the Observable will operate.
                                                     // Callable and observer method will be called on it
                                                     // If observeOn is used afterwards, the observer's methods
                                                     // will be called on it.
                                                     // Useful for IO operations such as reading a file
                                                     // or reading from database
                .subscribe(x -> {
                    long observerRunningThread = Thread.currentThread().getId();
                    System.out.println("D2. Thread ID: " + observerRunningThread + ". Item: " + x);
                }); // Runs on the same thread as the callable

        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        d1.dispose();
        d2.dispose();
    }

}
