package com.acme.rxjava.demo.operator;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;

public class MapOperatorDemo {

    public static void main(String[] args) {
        // Map applies the given function to the item emitted by the source observable
        // and emits this function's result.
        // Here, it is being used to map("transform") the integers into String with a format
        Disposable disposable = Observable.fromArray(1, 2, 3, 4, 5, 6)
                .map(x -> "Item " + x + ": " + x * 10) // "converts" integer to Strings
                .subscribe(System.out::println);

        disposable.dispose();
    }

}
