package com.acme.rxjava.demo.operator;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;

import java.util.concurrent.TimeUnit;

public class DebounceOperatorDemo {

    public static void main(String[] args) {
        // Will wait for 250ms to emit the item to the Observer
        // If the source emits a new item, it will wait more 250ms and so on.
        // In this case, we will have the number 6 printed 250ms after it was emitted from the source observable
        Disposable disposable = Observable.fromArray(1, 2, 3, 4, 5, 6)
                .debounce(250, TimeUnit.MILLISECONDS)
                .subscribe(System.out::println);

        disposable.dispose();
    }

}
