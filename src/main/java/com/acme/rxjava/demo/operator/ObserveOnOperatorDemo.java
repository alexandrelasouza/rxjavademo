package com.acme.rxjava.demo.operator;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class ObserveOnOperatorDemo {

    public static void main(String[] args) {
        // ObserveOn defines the thread in which the Observer will operate.
        // It allows switching threads for different kinds of processing.
        // It is possible to process IO using a thread from the "io" thread pool
        // It is possible to use the "computation" thread pool to perform more blocking tasks
        //
        // E.g: In Android, you can use the "computation" thread pool to perform a blocking task in
        // an Activity and then switch back to the "main thread" to perform UI updates.
        long threadId = Thread.currentThread().getId();
        System.out.println("Main Thread ID: " + threadId);

        Observable<Integer> intStream = Observable.fromArray(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

        Disposable disposable = intStream
                .observeOn(Schedulers.computation())
                .subscribe(x -> {
                    long observerRunningThread = Thread.currentThread().getId();
                    System.out.println("Observer. Thread ID: " + observerRunningThread + ". Item: " + x);
                }); // Observer will run on a thread provided by the computation thread pool

        try {
            // Need to stop the main thread so the background processing can end
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        disposable.dispose();
    }

}
