package com.acme.rxjava.demo.operator;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;

public class FilterOperatorDemo {

    public static void main(String[] args) {
        // It will emit the item only if it matches the Predicate
        // In this case, only numbers divisible by 2 will be emitted to the observer
        Disposable disposable = Observable.fromArray(1, 2, 3, 4, 5, 6)
                .filter(x -> x%2 == 0) // filters the stream to emit only numbers divisible by 2
                .subscribe(System.out::println);

        disposable.dispose();
    }

}
