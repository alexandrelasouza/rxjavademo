package com.acme.rxjava.demo.subjects;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.subjects.AsyncSubject;

public class AsyncSubjectDemo {

    public static void main(String[] args) {

        // The AsyncSubject emits ONLY the last emitted value by the source
        AsyncSubject<Integer> intAsyncSubject = AsyncSubject.create();
        Observable<Integer> source = Observable.fromArray(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

        CompositeDisposable compositeDisposable = new CompositeDisposable();

        compositeDisposable.add(intAsyncSubject.subscribe(x -> System.out.println("D1: " + x)));
        compositeDisposable.add(intAsyncSubject.subscribe(x -> System.out.println("D2: " + x)));

        source.subscribe(intAsyncSubject);

        compositeDisposable.dispose();

        asyncSubjectWithErrorDemo();
    }

    private static void asyncSubjectWithErrorDemo() {
        AsyncSubject<Integer> asyncSubject = AsyncSubject.create();
        Observable<Integer> source = Observable.create(new ObservableOnSubscribe<Integer>() {

            private int[] data = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

            @Override
            public void subscribe(ObservableEmitter<Integer> e) throws Exception {
                for (int value : data) {
                    e.onNext(value);

                    if (value == 5) {
                        throw new IllegalArgumentException("Value can't be 5");
                    }
                }
            }
        });

        // If the source terminates with an error, it won't emit anything but the error
        asyncSubject.subscribe(x -> System.out.println("No error occurred! YAY!"),
                t -> System.out.println("Error: " + t.getMessage()));

        source.subscribe(asyncSubject);
    }

}
