package com.acme.rxjava.demo.subjects;

import io.reactivex.subjects.UnicastSubject;

public class UnicastSubjectDemo {

    public static void main(String[] args) {
        // The UnicastSubject has a single subscriber/observer during
        // its lifetime. Any other subscriber will get:
        // A-) IllegalStateException if the subject has not finished yet
        // B-) Terminal event (error or completion) if the subject has already finished
        UnicastSubject<Integer> intUnicastSubject = UnicastSubject.create();

        intUnicastSubject.subscribe(System.out::println);

        // Will get an exception and be disposed immediately
        intUnicastSubject.subscribe(
                System.out::println,
                t -> System.out.println("onError: " + t.getMessage())
        );

        for (int i = 0; i < 6; i++) {
            intUnicastSubject.onNext(i);
        }

        // onComplete will dispose the registered observer
        intUnicastSubject.onComplete();
    }

}
