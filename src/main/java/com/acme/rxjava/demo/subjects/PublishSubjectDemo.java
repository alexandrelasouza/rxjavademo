package com.acme.rxjava.demo.subjects;

import io.reactivex.subjects.PublishSubject;

public class PublishSubjectDemo {

    public static void main(String[] args) {
        // The PublishSubject will emit all subsequently observed items to the subscriber.
        PublishSubject<Integer> publisher = PublishSubject.create();

        // These values will not be emitted. PublishSubject will not buffer them until
        // an observer has subscribed.
        for (int i = 0; i < 5; i++) {
            publisher.onNext(i);
        }

        publisher.subscribe(x -> System.out.println("D1: " + x));

        // D1 will receive these values
        for (int i = 5; i < 10; i++) {
            publisher.onNext(i);
        }

        publisher.subscribe(x -> System.out.println("D2: " + x));

        // D1 and D2 will receive these values
        for (int i = 10; i <= 15; i++) {
            publisher.onNext(i);
        }

        // onComplete will dispose all subscribers
        publisher.onComplete();
    }

}
