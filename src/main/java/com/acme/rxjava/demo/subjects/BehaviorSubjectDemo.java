/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acme.rxjava.demo.subjects;

import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.BehaviorSubject;

/**
 *
 * @author souzaala
 */
public class BehaviorSubjectDemo {

    public static void main(String[] args) {        
        
        // The BehaviorSubject emits the last emitted value and subsequent 
        // values to a new obserser
        BehaviorSubject<Integer> subject = BehaviorSubject.create();
        subject.onNext(0);
        
        // D1 will get 0, the last emitted value prior to its subscription
        Disposable d1 = subject.subscribe(x -> System.out.println("D1: " + x));
        
        for (int value = 1; value < 10; value++) {
            // Send values from 1 to 9 to current observer
            subject.onNext(value);
        }
        
        // D2 will get the last number emitted in the loop above: 9
        // and subsequent values
        System.out.println("\n\n\n");
        System.out.println("Subscribing new observer");
        Disposable d2 = subject.subscribe(x -> System.out.println("D2: " + x));
                      
        // D1 and D2 will get 10
        subject.onNext(10);
        
        subject.onComplete();
    }
    
}
