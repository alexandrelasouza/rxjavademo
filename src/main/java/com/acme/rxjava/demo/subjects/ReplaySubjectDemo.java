/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acme.rxjava.demo.subjects;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.subjects.ReplaySubject;

/**
 *
 * @author souzaala
 */
public class ReplaySubjectDemo {
    
    public static void main(String[] args) {
        Observable<Integer> source = Observable.fromArray(1, 2, 3, 4, 5, 6);        
        ReplaySubject<Integer> subject = ReplaySubject.create();
        CompositeDisposable compositeDisposable = new CompositeDisposable();
        
        System.out.println("Registering D1");
        compositeDisposable.add(subject
                .subscribe(x -> System.out.println("D1: " + x)));
        
        System.out.println("Subscribing subject to source");
        source.subscribe(subject);
        
        System.out.println("Registering D2");
        compositeDisposable.add(subject
                .subscribe(x -> System.out.println("D2: " + x)));       
        
        compositeDisposable.dispose();
    }
    
}
